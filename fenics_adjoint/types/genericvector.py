import backend
from .compat import gather

@staticmethod
def _ad_to_list(m):
    return gather(m)


backend.GenericVector._ad_to_list = _ad_to_list
